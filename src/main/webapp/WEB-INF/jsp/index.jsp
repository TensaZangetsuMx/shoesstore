<%-- 
    Document   : index
    Created on : 3/09/2015, 01:27:51 PM
    Author     : alfredo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request" />
<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="uri" value="${req.requestURI}" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

        <link rel="stylesheet" type="text/css" href="${cp}/resources/css/site.css" />
        <script src="${cp}/resources/js/js.js"></script>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="${cp}/resources/bootstrap/css/bootstrap.min.css">
        <script src="${cp}/resources/bootstrap/js/bootstrap.min.js"></script>
        
    </head>
    <body>
        <div class="center-block" style="text-align: center">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h2 class="panel-title">Iniciar sesión</h2>
                </div>
                <c:if test="${invalid}">
                <div class="alert alert-danger" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    ${msjError}
                </div>
                </c:if>
                <form class="form-horizontal" id="LoginForm" name="LoginForm" action="${cp}/inicio/login" method="post">
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="usuario" class="col-sm-3 control-label">
                                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                            </label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="username" name="username" placeholder="Usuario"
                                       value="${username}"
                                >
                            </div>
                            <label class="col-sm-2 control-label"></label>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-sm-3 control-label">
                                <span class="glyphicon glyphicon-console" aria-hidden="true"></span>
                            </label>
                            <div class="col-sm-7">
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password"
                                       value="${password}"   
                                >
                            </div>
                            <label class="col-sm-2 control-label"></label>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-success" name="btn_submitLogin" value="Log-In">
                            Entrar &nbsp;&nbsp;<span class="glyphicon glyphicon-log-in" aria-hidden="true"></span>
                        </button>
                    </div>
                </form>
            </div>
        </div>

    </body>
</html>
