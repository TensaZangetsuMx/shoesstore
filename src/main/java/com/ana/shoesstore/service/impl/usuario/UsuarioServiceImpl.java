package com.ana.shoesstore.service.impl.usuario;

import com.ana.shoesstore.dao.usuario.UsuarioDao;
import com.ana.shoesstore.dto.usuario.UsuarioDto;
import com.ana.shoesstore.service.usuario.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author alfredo
 */
@Service("usuarioService")
@Transactional
public class UsuarioServiceImpl implements UsuarioService {
    
    @Autowired 
    private UsuarioDao usuarioDao;
    
    @Override
    public UsuarioDto authenticate(String username, String Password) throws Exception {
        return usuarioDao.getByUsrPwd(username, Password);
    }
    
}
