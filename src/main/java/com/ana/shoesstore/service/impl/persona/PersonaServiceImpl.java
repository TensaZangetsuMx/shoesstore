package com.ana.shoesstore.service.impl.persona;

import com.ana.shoesstore.dao.persona.PersonaDao;
import com.ana.shoesstore.dto.persona.PersonaDto;
import com.ana.shoesstore.service.persona.PersonaService;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author alfredo
 */
@Service("personaService")
@Transactional
public class PersonaServiceImpl implements PersonaService {
    
    final static Logger log = Logger.getLogger(PersonaServiceImpl.class);
    
    @Autowired
    private PersonaDao personaDao;
    
    @Override
    public PersonaDto findById(int id) {
        return personaDao.findById(id);
    }
    
    @Override
    public ArrayList<PersonaDto> findAll() {
        ArrayList<PersonaDto> list = new ArrayList(); 
        try {
            list = personaDao.findAll();
            log.info(">> list.size(): " + list.size());
            for (PersonaDto dto : list) {
                log.info(">> " + dto.toString());
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
    
}
