package com.ana.shoesstore.service.persona;

import com.ana.shoesstore.dto.persona.PersonaDto;
import java.util.ArrayList;

/**
 *
 * @author alfredo
 */
public interface PersonaService {

    PersonaDto findById(int id);
    ArrayList<PersonaDto> findAll();
    
}
