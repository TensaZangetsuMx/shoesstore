package com.ana.shoesstore.service.usuario;

import com.ana.shoesstore.dto.usuario.UsuarioDto;

/**
 *
 * @author alfredo
 */
public interface UsuarioService {

    UsuarioDto authenticate(String username, String Password) throws Exception;
    
}
