package com.ana.shoesstore.util;

/**
 *
 * @author alfredo
 */
public class Constantes {
    
    public final static String TBL_SUB_FIX = "tbl_";
    public final static String TABLA_PERSONA = TBL_SUB_FIX + "persona";
    public final static String TABLA_USUARIO = TBL_SUB_FIX + "usuario";
    public final static String TABLA_ESTADO = TBL_SUB_FIX + "estado";
    
}
