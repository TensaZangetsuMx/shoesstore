package com.ana.shoesstore.controller;

import com.ana.shoesstore.dto.usuario.UsuarioDto;
import com.ana.shoesstore.service.usuario.UsuarioService;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author alfredo
 */
@Controller
public class DefaultController {
    
    final static Logger log = Logger.getLogger(DefaultController.class);
    
    @Autowired
    private UsuarioService usuarioService;
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(ModelMap map) {
        return "index";
    }
    
    @RequestMapping(value = "/inicio/login", method = RequestMethod.POST)
    public String login(ModelMap map, HttpServletRequest rquest) {
        String username = rquest.getParameter("username");
        String password = rquest.getParameter("password");
        log.info(">> username: " + username);
        log.info(">> password: " + password);
        
        try {
            UsuarioDto user = usuarioService.authenticate(username, password);
            log.info(">> usuario logueado: " + user.toString());
        }
        catch (Exception e) {
            log.info(">> Ocurrio un error: " + e);
        }
        
        return "index";
    }
    
}
