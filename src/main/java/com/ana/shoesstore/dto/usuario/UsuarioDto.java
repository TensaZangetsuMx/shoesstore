package com.ana.shoesstore.dto.usuario;

import com.ana.shoesstore.dto.estado.EstadoDto;
import com.ana.shoesstore.dto.persona.PersonaDto;
import com.ana.shoesstore.util.Constantes;
import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = Constantes.TABLA_USUARIO)
public class UsuarioDto implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_usuario")
    private Long idUsuario;
    
    @Column(name = "id_perfil_sistema")
    private Long idPerfilSistema;
    
    @ManyToOne
    @JoinColumn(name = "id_persona")
    private PersonaDto personaDto;
    
    @Column(name = "nb_usuario")
    private String username;
    
    @Column(name = "nb_password")
    private String password;
    
    @ManyToOne
    @JoinColumn(name = "id_estado")
    private EstadoDto estadoDto;
    
    @Column(name = "st_change_pwd")
    private String stChahgePwd;
    
    @Column(name = "fh_alta")
    private Timestamp fhAlta;
    
    @Column(name = "fh_baja")
    private Timestamp fhBaja;    
    
    @Override
    public String toString() {
        return "UsuarioDto = [id_usuario: " + idUsuario 
                + ", idPerfilSistema: " + idPerfilSistema 
                + ", personaDto: " + personaDto
                + ", username: " + username 
                + ", password: " + password 
                + ", estadoDto: " + estadoDto
                + ", stChahgePwd: " + stChahgePwd 
                + ", fhAlta: " + fhAlta 
                + ", fhBaja: " + fhBaja
            + "]";
    }
    
}
