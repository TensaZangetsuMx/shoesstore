package com.ana.shoesstore.dto.persona;

import com.ana.shoesstore.util.Constantes;
import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author alfredo
 */
@Entity
@Table(name = Constantes.TABLA_PERSONA)
public class PersonaDto implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_persona")
    private Long idPersona;
    
    @Column(name = "nb_nombre")
    private String nbNombre;
    
    @Column(name = "nb_ap_paterno")
    private String nbApPaterno;
    
    @Column(name = "nb_ap_materno")
    private String nbApMaterno;
    
    @Column(name = "nb_sexo")
    private String nbSexo;
    
    @Column(name = "nu_tel_casa")
    private Long nuTelCasa;
    
    @Column(name = "nu_tel_celular")
    private Long nuTelCelular;
    
    @Column(name = "nb_correo")
    private String nbCorreo;
    
    @Column(name = "nb_direccion")
    private String nbDireccion;
    
    @Column(name = "fh_alta")
    private Timestamp fhAlta;
    
    @Column(name = "fh_baja")
    private Timestamp fhBaja;
    
    @Override
    public String toString() {
        return "PersonaDto = [id_persona: " + idPersona 
                + ", nb_nombre: " + nbNombre
                + ", nb_ap_paterno: " + nbApPaterno
                + ", nb_ap_materno: " + nbApMaterno
                + ", nb_sexo: " + nbSexo
                + ", nu_tel_casa: " + nuTelCasa
                + ", nu_tel_celular: " + nuTelCelular
                + ", nb_correo: " + nbCorreo
                + ", nb_direccion: " + nbDireccion
                + ", fh_alta: " + fhAlta
                + ", fh_baja: " + fhBaja
            + "]";
    }

    public Long getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Long idPersona) {
        this.idPersona = idPersona;
    }

    public String getNbNombre() {
        return nbNombre;
    }

    public void setNbNombre(String nbNombre) {
        this.nbNombre = nbNombre;
    }

    public String getNbApPaterno() {
        return nbApPaterno;
    }

    public void setNbApPaterno(String nbApPaterno) {
        this.nbApPaterno = nbApPaterno;
    }

    public String getNbApMaterno() {
        return nbApMaterno;
    }

    public void setNbApMaterno(String nbApMaterno) {
        this.nbApMaterno = nbApMaterno;
    }

    public String getNbSexo() {
        return nbSexo;
    }

    public void setNbSexo(String nbSexo) {
        this.nbSexo = nbSexo;
    }

    public Long getNuTelCasa() {
        return nuTelCasa;
    }

    public void setNuTelCasa(Long nuTelCasa) {
        this.nuTelCasa = nuTelCasa;
    }

    public Long getNuTelCelular() {
        return nuTelCelular;
    }

    public void setNuTelCelular(Long nuTelCelular) {
        this.nuTelCelular = nuTelCelular;
    }

    public String getNbCorreo() {
        return nbCorreo;
    }

    public void setNbCorreo(String nbCorreo) {
        this.nbCorreo = nbCorreo;
    }

    public String getNbDireccion() {
        return nbDireccion;
    }

    public void setNbDireccion(String nbDireccion) {
        this.nbDireccion = nbDireccion;
    }

    public Timestamp getFhAlta() {
        return fhAlta;
    }

    public void setFhAlta(Timestamp fhAlta) {
        this.fhAlta = fhAlta;
    }

    public Timestamp getFhBaja() {
        return fhBaja;
    }

    public void setFhBaja(Timestamp fhBaja) {
        this.fhBaja = fhBaja;
    }

    
    
}
