package com.ana.shoesstore.dto.estado;

import com.ana.shoesstore.util.Constantes;
import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author alfredo
 */
@Entity
@Table(name = Constantes.TABLA_ESTADO)
public class EstadoDto implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_estado")
    private Long idEstado;
    
    @Column(name = "nb_estado")
    private String nbEstado;
    
    @Column(name = "tx_comentario")
    private String txComentario;
    
    @Column(name = "st_activo")
    private Boolean stActivo;
    
    @Column(name = "fh_alta")
    private Timestamp fhAlta;
    
    @Column(name = "fh_baja")
    private Timestamp fhBaja;

    @Override
    public String toString() {
        return "EstadoDto = [id_estado: " + idEstado
                + ", nb_estado: " + nbEstado
                + ", txComentario: " + txComentario
                + ", stActivo: " + stActivo
                + ", fhAlta: " + fhAlta
                + ", fhBaja: " + fhBaja
            + "]";
    }
    
    public Long getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Long idEstado) {
        this.idEstado = idEstado;
    }

    public String getNbEstado() {
        return nbEstado;
    }

    public void setNbEstado(String nbEstado) {
        this.nbEstado = nbEstado;
    }

    public String getTxComentario() {
        return txComentario;
    }

    public void setTxComentario(String txComentario) {
        this.txComentario = txComentario;
    }

    public Boolean getStActivo() {
        return stActivo;
    }

    public void setStActivo(Boolean stActivo) {
        this.stActivo = stActivo;
    }

    public Timestamp getFhAlta() {
        return fhAlta;
    }

    public void setFhAlta(Timestamp fhAlta) {
        this.fhAlta = fhAlta;
    }

    public Timestamp getFhBaja() {
        return fhBaja;
    }

    public void setFhBaja(Timestamp fhBaja) {
        this.fhBaja = fhBaja;
    }
    
}
