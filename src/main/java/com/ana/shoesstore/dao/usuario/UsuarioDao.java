package com.ana.shoesstore.dao.usuario;

import com.ana.shoesstore.dto.usuario.UsuarioDto;

/**
 *
 * @author alfredo
 */
public interface UsuarioDao {

    UsuarioDto getByUsrPwd(String username, String password) throws Exception;
    
}
