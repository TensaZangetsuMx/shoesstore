package com.ana.shoesstore.dao.impl.usuario;

import com.ana.shoesstore.dao.AbstractDao;
import com.ana.shoesstore.dao.usuario.UsuarioDao;
import com.ana.shoesstore.dto.usuario.UsuarioDto;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 *
 * @author alfredo
 */
@Repository("usuarioDao")
public class UsuarioDaoImpl extends AbstractDao<Integer, UsuarioDto> implements UsuarioDao {
    
    @Override
    public UsuarioDto getByUsrPwd(String username, String password) throws Exception {
        Criteria c = createEntityCriteria();
        c.add(Restrictions.eq("username", username));
        c.add(Restrictions.eq("password", password));
        
        if (c.list().size() != 1) {
            return null;
        } else {
            return (UsuarioDto)c.uniqueResult();
        }        
    }
    
}
