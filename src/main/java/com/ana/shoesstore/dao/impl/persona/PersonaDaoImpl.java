package com.ana.shoesstore.dao.impl.persona;

import com.ana.shoesstore.dao.AbstractDao;
import com.ana.shoesstore.dao.persona.PersonaDao;
import com.ana.shoesstore.dto.persona.PersonaDto;
import java.util.ArrayList;
import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;

/**
 *
 * @author alfredo
 */
@Repository("personaDao")
public class PersonaDaoImpl extends AbstractDao<Integer, PersonaDto> implements PersonaDao {

    @Override
    public PersonaDto findById(int id) {
        return getByKey(id);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public ArrayList<PersonaDto> findAll() throws Exception {
        Criteria criteria = createEntityCriteria();
        return (ArrayList<PersonaDto>) criteria.list();
    }
    
}
