package com.ana.shoesstore.dao.persona;

import com.ana.shoesstore.dto.persona.PersonaDto;
import java.util.ArrayList;

/**
 *
 * @author alfredo
 */
public interface PersonaDao {
    
    PersonaDto findById(int id);
    ArrayList<PersonaDto> findAll() throws Exception;
    
}
